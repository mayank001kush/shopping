package testcase;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import base.BaseTest;
import utilities.ReadXLSData;

public class MyFirstTest extends BaseTest{

	
	@Test(dataProviderClass = ReadXLSData.class, dataProvider = "testdata")
	public static void LoginTest(String username, String password) {
		
		
		driver.findElement(By.linkText(loc.getProperty("signin_link"))).click();
		driver.findElement(By.id(loc.getProperty("email_field"))).sendKeys(username);
		driver.findElement(By.id(loc.getProperty("next_button"))).click();
		driver.findElement(By.xpath(loc.getProperty("pwd_field"))).sendKeys(password);
		driver.findElement(By.xpath(loc.getProperty("login_next_button"))).click();
		
	
	}
	

}
